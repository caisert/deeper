module.exports = function(api) {
    api.cache(false);
    const presets = [
        [
            '@babel/env',
            {
                'targets': {'browsers': ['last 2 versions', 'safari >= 7']},
            },
        ],
    ];
    const plugins = [
        // EntryTarget
        '@babel/plugin-transform-classes',

        // [EntryTarget, EntryOptions]
        ['@babel/plugin-transform-arrow-functions', {spec: true}],

        // [EntryTarget, EntryOptions, string]
        ['@babel/plugin-transform-for-of', {loose: true}, 'some-name'],

        // ConfigItem
        // babel.createConfigItem(require("@babel/plugin-transform-spread")),
    ];

    return {
        presets,
        plugins,
    };
};
