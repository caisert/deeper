# Deeper Clothing Store

Sistema de tienda online.

## Comandos ejecutables de la aplicación:

### 1. Ejecutar en modo desarrollo: 
Ejecutar la aplicación con los sources map para debug
```
npm run start:dev
```

### 2. Ejecutar tests:
Ejecutar los test de forma indefinida
```
npm test
```

### 3. Ejecutar para deploy en servidor:
Preparar los archivos comprimidos que irán al servidor de producción
```
npm build
```