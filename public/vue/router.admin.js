import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import admin from './templates/admin.vue';
import login from './templates/pages/login.vue'; 
import orders from './templates/pages/orders.vue';
import products from './templates/pages/products.vue';
import subscribers from './templates/pages/subscribers.vue';

const router = new VueRouter({   
    routes: [
        {
            path: '/',
            component:orders,
        },
        {
            path: '/subscribers',
            name: 'subscribers',
            component:subscribers,
        },
        {
            path: '/products',
            name: 'products',
            component:products,
        },
        {
            path: '/login',
            name: 'login',
            component:login,
        },,
        {
            path:"*",
            redirect:"/",
        },
    ] 
});
new Vue({
    router,
    render: createEle => createEle(admin)
}).$mount('#app')