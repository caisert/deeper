import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import index from './templates/index.vue';
import error404 from './templates/pages/404.vue';
import home from './templates/pages/home.vue';
import nosotros from './templates/pages/nosotros.vue';
import shop from './templates/pages/shop.vue';
import itemDetails from './templates/components/item-details.vue';
import contactus from './templates/pages/contact-us.vue';
import termsCondition from './templates/pages/terms-condition.vue';
import shippingPolicy from './templates/pages/shipping-policy.vue';

const router = new VueRouter({   
    routes: [
        {
            path: '/',
            name: 'home',
            component:home,
        },
        {
            path: '/nosotros',
            name: 'nosotros',
            component:nosotros,
        },
        {
            path: '/contact-us',
            name: 'contact-us',
            component:contactus,
        },
        {
            path: '/shop',
            name: 'shop',
            component:shop,
            children:[
                {
                    path: ':id_products',
                    name: 'itemDetails',
                    component:itemDetails,
                    props:true,
                },
            ],
        },
        {
            path: '/terms-condition',
            name: 'terms-condition',
            component:termsCondition,
        },
        {
            path: '/shipping-policy',
            name: 'shipping-policy',
            component:shippingPolicy,
        },
        {
            path:'/404',
            name:'404',
            component:error404,
        },
        {
            path:"*",
            redirect:"/404",
        }
    ] 
});
new Vue({
    router,
    render: createEle => createEle(index)
}).$mount('#app')