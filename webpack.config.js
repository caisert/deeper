const webpack = require('webpack');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {VueLoaderPlugin} = require('vue-loader');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = function(env, options) {
  return {
    mode: options.mode,
    devtool: (options.mode == 'production') ? 'source-maps' : 'eval',
    entry: {
      index: './public/vue/router.js',
      admin: './public/vue/router.admin.js',
    },
    output: {
      filename: '[name].bundle.js',
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: 'vue-loader',
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
            },
          },
        },
        {
          test: /\.less$/,
          use: [{
              loader: 'style-loader',
          }, {
              loader: 'css-loader', options: {
                  sourceMap: true,
              },
          }, {
              loader: 'less-loader', options: {
                  sourceMap: true,
              },
          }],
        },
        {
          test: /\.(png|jpg|gif|webp)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
              },
            },
          ],
        },
      ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),

        // Comentado por incluir ambos bundles (admin y index) en el mismo index.html
        // new HtmlWebpackPlugin({
        //     filename: 'index.html',
        //     template: './public/index.html',
        // }),

        new CopyWebpackPlugin([
          {from: './public/index.html'},
          {from: './public/admin/index.html', to: 'admin/'},
          {from: './public/multimedia/', to: 'multimedia/'},
          {from: './public/js/', to: 'js/'},
          {from: './public/manifest.json'},
          {from: './public/robots.txt'},
          /*
          {from: './public/manifest.webapp'},
          {from: './public/sitemap.xml'},
          {from: './public/stylesheets/fonts/'}, */
          {from: './public/favicon.jpg'},
          {from: './public/api.php'},
        ]),

        new VueLoaderPlugin(),
        // new webpack.ProvidePlugin({
        //     $: 'jquery',
        //     jQuery: 'jquery',
        // }),
    ],
  };
};
