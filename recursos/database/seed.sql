-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-01-2019 a las 22:44:49
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `deeper`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id_orders` int(15) NOT NULL,
  `date_orders` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_persons_orders` int(15) NOT NULL,
  `address_orders` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `city_orders` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `zip_orders` int(6) NOT NULL,
  `country_orders` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `phone_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `status_orders` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tracking_orders` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `total_amount_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `paypal_orders` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `method_orders` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `created_orders` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_orders` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id_orders`, `date_orders`, `id_persons_orders`, `address_orders`, `city_orders`, `zip_orders`, `country_orders`, `phone_orders`, `status_orders`, `tracking_orders`, `total_amount_orders`, `paypal_orders`, `method_orders`, `created_orders`, `modified_orders`) VALUES
(1, '2018-12-27 04:00:59', 1, 'Calle principal en la segunda entrada a la derecha . Cuidado con el perro.', 'Valtimore', 654, '5', '0498 645822', 'Sent', 'FX x1ab', '416', 'data.paymentID', '', '2018-12-11 08:51:02', '2018-12-27 04:00:59'),
(4, '2018-12-27 03:59:36', 2, '4741 N road', 'Sew York', 1122, '5', '654 789 789', 'Not yet shipped', 'fedasdf=ss', '251', 'data.paymentID', '', '2018-12-14 06:49:06', '2018-12-27 03:59:36'),
(5, '2018-12-27 04:01:49', 3, 'CC paseo de las ferias', 'Bogota', 444, '35', '+57 111 22 33', 'Not yet shipped', '', '121', 'PAY-50B835898J9878014LQJVXWY', 'paypal', '2018-12-14 07:30:45', '2018-12-27 04:01:49'),
(15, '2018-12-27 04:01:57', 8, 'cal', 'lo', 12, '5', '414', 'Not yet shipped', '', '79', '', '', '2018-12-15 20:17:14', '2018-12-27 04:01:57'),
(16, '2018-12-27 03:59:48', 8, 'als', 'loc', 111, '5', '414', 'Not yet shipped', '', '91', '', '', '2018-12-15 20:18:35', '2018-12-27 03:59:48'),
(17, '2018-12-15 20:20:17', 10, 'cal', 'nuru', 111, '35', '412', 'Envio pendiente', '', '114', '', '', '2018-12-15 20:20:17', '0000-00-00 00:00:00'),
(19, '2018-12-27 05:14:57', 12, 'sas as asd s', 'asdf', 111, '35', '5555', 'Not yet shipped', '', '114', 'PAY-9A994783X9356274NLQKWGQA', 'paypal', '2018-12-15 20:26:49', '2018-12-27 05:14:57'),
(25, '2018-12-27 04:00:48', 15, 'a', 'a', 111, '5', 'a', 'Sent', '', '79', '', '', '2018-12-20 23:13:16', '2018-12-27 04:00:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_products`
--

CREATE TABLE `orders_products` (
  `id_orders_products` int(15) NOT NULL,
  `id_orders_orders_products` int(15) NOT NULL,
  `id_products_variants_orders_products` int(11) NOT NULL,
  `price_orders_products` decimal(15,2) NOT NULL,
  `quantity_orders_products` int(11) NOT NULL,
  `gift_orders_products` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `created_orders_products` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `orders_products`
--

INSERT INTO `orders_products` (`id_orders_products`, `id_orders_orders_products`, `id_products_variants_orders_products`, `price_orders_products`, `quantity_orders_products`, `gift_orders_products`, `created_orders_products`) VALUES
(1, 1, 15, '74.00', 1, '0', '2018-12-11 08:51:02'),
(2, 1, 18, '79.00', 1, '0', '2018-12-11 08:51:02'),
(3, 1, 11, '86.00', 3, '0', '2018-12-11 08:51:02'),
(4, 4, 20, '74.00', 1, '0', '2018-12-14 06:49:06'),
(5, 4, 11, '86.00', 2, '1', '2018-12-14 06:49:06'),
(6, 5, 25, '86.00', 1, '0', '2018-12-14 07:30:45'),
(16, 15, 22, '74.00', 1, '0', '2018-12-15 20:17:14'),
(17, 16, 10, '86.00', 1, '0', '2018-12-15 20:18:35'),
(18, 17, 30, '79.00', 1, '0', '2018-12-15 20:20:17'),
(20, 19, 19, '79.00', 1, '0', '2018-12-15 20:26:49'),
(26, 25, 20, '74.00', 1, '0', '2018-12-20 23:13:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persons`
--

CREATE TABLE `persons` (
  `id_persons` int(11) NOT NULL,
  `name_persons` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `last_persons` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email_persons` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `phone_persons` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `created_persons` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persons`
--

INSERT INTO `persons` (`id_persons`, `name_persons`, `last_persons`, `email_persons`, `phone_persons`, `created_persons`) VALUES
(1, 'Carmensa', 'Carnaval', 'ca@rm.en', '0478 1231212', '2018-12-11 08:51:02'),
(2, 'Silvia', 'Sanchez', 'silvia@sanch.ez', '480 980 81 82', '2018-12-14 06:49:05'),
(3, 'Pedro', 'Perez', 'pedro@per.ez', '+57 555 555 55', '2018-12-14 07:30:45'),
(6, 'Carmencita', '', 'carm@en.ita', '', '2018-12-14 18:23:59'),
(7, 'a', 's', 's@v.v', 's', '2018-12-14 19:11:59'),
(8, 'Local', 'Storage', 'lo@ca.l', '11426', '2018-12-15 20:17:14'),
(10, 'Local 3', 'alsdj', 'lo@sto.sl', '426', '2018-12-15 20:20:17'),
(11, 'local 4', 'asdf', 'aa@ss.cc', 'ASF', '2018-12-15 20:21:04'),
(12, 'Local pay', 'asdf', 'aa@aa.ccc', 'aasdf', '2018-12-15 20:26:49'),
(13, 'Angelo', 'R', 'admin@deeperclothing.com', '', '2018-12-16 06:35:00'),
(14, '', '', '', '', '2018-12-20 21:33:48'),
(15, 'asd', 'a', 'a@b.c', 'a', '2018-12-20 23:13:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persons_admin`
--

CREATE TABLE `persons_admin` (
  `id_persons_admin` int(15) NOT NULL,
  `id_persons_persons_admin` int(15) NOT NULL,
  `clave_persons_admin` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `created_persons_admin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_persons_admin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `persons_admin`
--

INSERT INTO `persons_admin` (`id_persons_admin`, `id_persons_persons_admin`, `clave_persons_admin`, `created_persons_admin`, `modified_persons_admin`) VALUES
(1, 1, '123', '2018-12-16 05:34:38', '0000-00-00 00:00:00'),
(2, 13, '123456', '2018-12-16 06:35:23', '2018-12-30 15:07:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id_products` int(15) NOT NULL,
  `name_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `code_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_products` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `price_products` decimal(15,2) NOT NULL,
  `description_products` text COLLATE utf8_spanish_ci NOT NULL,
  `disabled_products` tinyint(4) NOT NULL DEFAULT '0',
  `created_products` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id_products`, `name_products`, `code_products`, `image_products`, `price_products`, `description_products`, `disabled_products`, `created_products`) VALUES
(1, 'ANGELIKA', 'DE101T', './multimedia/products/DE101T.png', '74.00', '', 1, '2018-12-21 05:09:27'),
(2, 'ISA', 'DE101TP', './multimedia/products/DE101TP.png', '74.00', '', 0, '2018-12-20 03:53:09'),
(3, 'MARINA', 'DE102T', './multimedia/products/DE102T.png', '74.00', '', 0, '2018-12-20 03:53:14'),
(4, 'CARESSA', 'DE103T', './multimedia/products/DE103T.png', '79.00', 'Keep it classy! silver tailored jacket with gold-tones and one-button closure, notched lapels and patch pockets at waist. All in one piece.', 0, '2018-12-20 03:53:18'),
(5, 'AUDRA', 'DE104T', './multimedia/products/DE104T.png', '78.00', '', 0, '2018-12-20 03:53:22'),
(6, 'GESA', 'DE105T', './multimedia/products/DE105T.png', '74.00', 'Where you go, wear this amazing look anytime and anywhere, the perfect mix of colors and design make you feel sporty, practical and comfortable. Fits Perfect.', 0, '2018-12-20 03:53:26'),
(7, 'KALENE', 'DE105TP', './multimedia/products/DE105TP.png', '74.00', 'Deeper reinvent this classic look giving you a new and fresh style with embroidered details that enhance the garment without compromising individuality and style.', 0, '2018-12-20 03:53:30'),
(8, 'REGINA', 'DE106T', './multimedia/products/DE106T.png', '74.00', 'Casual and romantic, handcrafted for truly one of a kind style. The soft washed denim look will make you stand out. This garment feature front silver-tone trimmings, all made in digital print.', 0, '2018-12-20 03:53:34'),
(9, 'ANTOINETTE', 'DE107T', './multimedia/products/DE107T.png', '86.00', 'Stand out! a print that take you to the limit, feel comfortable with elegance and style, tailored for a perfect fit and luxurious look that bring a touch of classic style.', 0, '2019-01-03 20:37:21'),
(10, 'BERNADETTE', 'DE108T', './multimedia/products/DE108T.jpg', '86.00', 'Feel special wearing this luxurious and stylish design, reinventing this classic coat to give you a new fresh fitted silhouette.', 0, '2018-12-24 15:03:46'),
(11, 'BRITANIA', 'DE109T', './multimedia/products/DE109T.jpg', '86.00', 'Designed to resemble a truly military essense, wear it with attitude and self expression. Long sleeve, two front packets and gold accents, putting attention to every detail to get the perfect look for this garment.', 0, '2018-12-24 15:03:59'),
(12, 'BRIGANTIA', 'DE110T', './multimedia/products/DE110T.png', '79.00', 'Refresh your wardrobe, be stylish and strike with this new creation from Deeper that you will be wearing almost everyday. A true must-have this season. We can guarantee it\'s a hit!', 0, '2019-01-03 21:36:51'),
(13, 'SIERRA', 'DE111T', './multimedia/products/DE111T.png', '79.00', 'Pure ethnic... geometric lines and details, all combined in one piece. Inmerse yourself into this exotic look.', 0, '2018-12-20 03:53:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id_products_attributes` int(15) NOT NULL,
  `name_products_attributes` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_attributes` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_attributes`
--

INSERT INTO `products_attributes` (`id_products_attributes`, `name_products_attributes`, `created_products_attributes`) VALUES
(1, 'Size', '2018-11-06 15:53:22'),
(2, 'Color', '2018-11-06 15:53:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_attr_values`
--

CREATE TABLE `products_attr_values` (
  `id_products_attr_values` int(15) NOT NULL,
  `id_products_attributes_products_attr_values` int(15) NOT NULL,
  `name_products_attr_values` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_attr_values` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_attr_values`
--

INSERT INTO `products_attr_values` (`id_products_attr_values`, `id_products_attributes_products_attr_values`, `name_products_attr_values`, `created_products_attr_values`) VALUES
(1, 1, 'S', '2018-11-06 15:53:58'),
(2, 1, 'M', '2018-11-06 15:53:58'),
(3, 1, 'L', '2018-11-06 15:54:19'),
(4, 1, 'XL', '2018-11-06 15:54:19'),
(5, 2, 'Blue', '2018-11-06 15:54:35'),
(6, 2, 'Green', '2018-11-06 15:54:35'),
(9, 2, 'Red', '2018-11-06 15:54:55'),
(10, 2, 'White', '2018-11-06 15:54:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_categories`
--

CREATE TABLE `products_categories` (
  `id_products_categories` int(15) NOT NULL,
  `id_parent_products_categories` int(15) NOT NULL,
  `name_products_categories` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `order_products_categories` int(15) NOT NULL,
  `created_products_categories` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_categories`
--

INSERT INTO `products_categories` (`id_products_categories`, `id_parent_products_categories`, `name_products_categories`, `order_products_categories`, `created_products_categories`) VALUES
(1, 0, 'DENNIM', 1, '2018-11-27 19:13:30'),
(2, 0, 'CLASSIC', 3, '2018-11-27 19:13:30'),
(3, 0, 'CASUAL', 2, '2018-11-27 19:13:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_gallery`
--

CREATE TABLE `products_gallery` (
  `id_products_gallery` int(15) NOT NULL,
  `id_slider_products_gallery` int(15) NOT NULL,
  `id_products_products_gallery` int(15) NOT NULL,
  `image_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_hd_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `alt_products_gallery` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Texto alternativo',
  `description_products_gallery` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `order_products_gallery` int(15) NOT NULL,
  `created_products_gallery` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `disabled_products_gallery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_gallery`
--

INSERT INTO `products_gallery` (`id_products_gallery`, `id_slider_products_gallery`, `id_products_products_gallery`, `image_products_gallery`, `image_hd_products_gallery`, `alt_products_gallery`, `description_products_gallery`, `order_products_gallery`, `created_products_gallery`, `disabled_products_gallery`) VALUES
(1, 1, 6, './multimedia/slides/DE105T.webp', './multimedia/slides/DE105T_F.webp', 'imagen slide primera', 'algo descriptivo para mostrar en slider', 5, '2018-12-11 03:31:51', 1),
(2, 1, 13, './multimedia/slides/de111t.png', './multimedia/slides/de111t.png', 'alternativo dos', 'texto descriptivo dos', 4, '2018-12-11 03:31:48', 1),
(3, 1, 10, './multimedia/slides/primera.jpg', './multimedia/slides/primera.jpg', 'bernadette style', 'Bernadette Jacket', 1, '2018-11-21 05:20:37', 0),
(4, 1, 4, './multimedia/slides/segunda.jpg', './multimedia/slides/segunda.jpg', 'caressa style', 'Caressa Jacket', 2, '2018-12-26 22:22:24', 0),
(5, 1, 5, './multimedia/slides/tercera.jpg', './multimedia/slides/tercera.jpg', 'audra style', 'Audra cover', 3, '2018-12-26 22:22:53', 0),
(6, 1, 10, './multimedia/slides/DE108T.webp', './multimedia/slides/DE108T_F.webp', 'Bernadette', 'Bernadette description', 6, '2018-12-11 03:31:39', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_images`
--

CREATE TABLE `products_images` (
  `id_products_images` int(15) NOT NULL,
  `id_products_variants_products_images` int(15) NOT NULL,
  `image_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `image_hd_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `alt_products_images` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `created_products_images` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `disabled_products_images` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_images`
--

INSERT INTO `products_images` (`id_products_images`, `id_products_variants_products_images`, `image_products_images`, `image_hd_products_images`, `alt_products_images`, `created_products_images`, `disabled_products_images`) VALUES
(1, 1, './multimedia/products/DE101T.png', '', '', '2018-10-17 05:08:20', 0),
(2, 1, './multimedia/products/DE101T_F.png', '', '', '2018-11-23 05:57:59', 0),
(3, 5, './multimedia/products/DE103T.png', './multimedia/products/DE103T.png', '', '2018-11-24 02:52:21', 0),
(4, 5, './multimedia/products/DE103T_F.jpg', './multimedia/products/DE103T_F.png', '', '2018-11-24 02:52:21', 0),
(5, 6, './multimedia/products/DE104T.png', '', '', '2018-11-24 02:52:53', 0),
(6, 6, './multimedia/products/DE104T_F.png', '', '', '2018-11-24 02:52:53', 0),
(7, 7, './multimedia/products/DE105T.png', '', '', '2018-11-24 02:53:44', 0),
(8, 7, './multimedia/products/DE105T_F.png', '', '', '2018-11-24 02:53:44', 0),
(9, 8, './multimedia/products/DE105TP.png', '', '', '2018-11-24 02:54:23', 0),
(10, 8, './multimedia/products/DE105TP_F.png', '', '', '2018-11-24 02:54:23', 0),
(11, 9, './multimedia/products/DE106T.png', '', '', '2018-11-24 02:54:48', 0),
(12, 9, './multimedia/products/DE106T_F.png', '', '', '2018-11-24 02:54:48', 0),
(13, 10, './multimedia/products/DE107T.png', '', '', '2018-11-24 02:55:23', 0),
(14, 10, './multimedia/products/DE107T_F.png', '', '', '2018-11-24 02:55:23', 0),
(15, 11, './multimedia/products/DE108T.jpg', '', '', '2018-11-24 02:55:46', 0),
(16, 11, './multimedia/products/DE108T_F.png', '', '', '2018-11-24 02:55:46', 0),
(17, 12, './multimedia/products/DE109T.jpg', '', '', '2018-11-24 02:56:26', 0),
(18, 12, './multimedia/products/DE109T_F.png', '', '', '2018-11-24 02:56:26', 0),
(19, 13, './multimedia/products/DE110T.png', '', '', '2018-11-24 02:56:52', 0),
(20, 13, './multimedia/products/DE110T_F.png', '', '', '2018-11-24 02:56:52', 0),
(23, 14, './multimedia/products/DE111T.png', '', '', '2018-11-24 02:57:32', 0),
(24, 14, './multimedia/products/DE111T_F.png', '', '', '2018-11-24 02:57:32', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_in_categories`
--

CREATE TABLE `products_in_categories` (
  `id_products_in_categories` int(15) NOT NULL,
  `id_products_categories_products_in_categories` int(15) NOT NULL,
  `id_products_products_in_categories` int(15) NOT NULL,
  `created_products_in_categories` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_in_categories`
--

INSERT INTO `products_in_categories` (`id_products_in_categories`, `id_products_categories_products_in_categories`, `id_products_products_in_categories`, `created_products_in_categories`) VALUES
(1, 1, 1, '2018-11-27 19:26:34'),
(2, 1, 2, '2018-11-27 19:26:34'),
(3, 1, 3, '2018-11-27 19:26:34'),
(4, 3, 4, '2018-11-27 19:26:34'),
(5, 1, 5, '2018-11-27 19:26:34'),
(6, 1, 6, '2018-11-27 19:26:34'),
(7, 1, 7, '2018-11-27 19:26:34'),
(8, 1, 8, '2018-11-27 19:26:34'),
(9, 2, 9, '2018-11-27 19:26:34'),
(10, 2, 10, '2018-11-27 19:26:34'),
(11, 1, 3, '2018-11-27 19:26:34'),
(12, 2, 11, '2018-11-27 19:26:34'),
(13, 3, 12, '2018-11-27 19:26:34'),
(14, 3, 13, '2018-11-27 19:26:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_variants`
--

CREATE TABLE `products_variants` (
  `id_products_variants` int(15) NOT NULL,
  `id_products_products_variants` int(15) NOT NULL,
  `quantity_products_variants` int(11) NOT NULL,
  `name_products_variants` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `code_products_variants` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `price_products_variants` decimal(15,2) NOT NULL,
  `disabled_products_variants` tinyint(1) NOT NULL DEFAULT '0',
  `created_products_variants` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_variants`
--

INSERT INTO `products_variants` (`id_products_variants`, `id_products_products_variants`, `quantity_products_variants`, `name_products_variants`, `code_products_variants`, `price_products_variants`, `disabled_products_variants`, `created_products_variants`) VALUES
(1, 1, 0, 'ANGELIKA size S', 'DE101T-S', '74.00', 0, '2018-11-02 19:12:18'),
(2, 1, 0, 'ANGELIKA size M', 'DE101T-M', '74.00', 0, '2018-11-06 19:52:59'),
(3, 2, 0, 'ISA size S', 'DE101TP-S', '74.00', 0, '2018-11-06 21:15:32'),
(4, 3, 0, 'Marina size S', 'DE102T-S', '74.00', 0, '2018-11-24 02:42:25'),
(5, 4, 8, 'CARESSA size S', 'DE103T-S', '79.00', 0, '2018-11-24 02:42:25'),
(6, 5, 0, 'AUDRA size S', 'DE104T-S', '78.00', 0, '2018-11-24 02:42:25'),
(7, 6, 5, 'GESA size S', 'DE105T-S', '74.00', 0, '2018-11-24 02:42:25'),
(8, 7, 8, 'KALENE size S', 'DE105TP-S', '74.00', 0, '2018-11-24 02:42:25'),
(9, 8, 6, 'REGINA size S', 'DE106T-S', '74.00', 0, '2018-11-24 02:45:25'),
(10, 9, 6, 'ANTOINETTE size S', 'DE107T-S', '86.00', 0, '2018-11-24 02:45:25'),
(11, 10, 8, 'BERNADETTE size S', 'DE108T-S', '86.00', 0, '2018-11-24 02:45:25'),
(12, 11, 3, 'BRITANIA size S', 'DE109T-S', '86.00', 0, '2018-11-24 02:45:25'),
(13, 12, 12, 'BRIGANTIA size S', 'DE110T-S', '79.00', 0, '2018-11-24 02:45:25'),
(14, 13, 8, 'SIERRA size S', 'DE111T-S', '79.00', 0, '2018-11-24 02:46:26'),
(15, 6, 5, 'GESA size M', 'DE105T-M', '74.01', 0, '2018-12-07 06:01:57'),
(18, 4, 7, 'CARESSA size M', 'DE103T-M', '79.00', 0, '2018-12-07 06:06:06'),
(19, 4, 3, 'CARESSA size L', 'DE103T-L', '79.00', 0, '2018-12-07 06:06:06'),
(20, 8, 5, 'REGINA size M', 'DE106T-M', '74.00', 0, '2018-12-07 06:10:18'),
(21, 8, 3, 'REGINA size L', 'DE106T-L', '74.00', 0, '2018-12-07 06:10:18'),
(22, 7, 5, 'KALENE size M', 'DE105TP-M', '74.00', 0, '2018-12-07 06:14:00'),
(23, 7, 3, 'KALENE size L', 'DE105TP-L', '74.00', 0, '2018-12-07 06:14:00'),
(24, 9, 6, 'ANTOINETTE size M', 'DE107T-M', '86.00', 0, '2018-12-07 06:16:41'),
(25, 9, 6, 'ANTOINETTE size L', 'DE107T-L', '86.00', 0, '2018-12-07 06:16:41'),
(26, 11, 2, 'BRITANIA size M', 'DE109T-M', '86.00', 0, '2018-12-07 06:20:25'),
(27, 11, 1, 'BRITANIA size L', 'DE109T-L', '86.00', 0, '2018-12-07 06:20:25'),
(28, 10, 3, 'BERNADETTE Talla M', 'DE108T-M', '86.00', 0, '2018-12-07 06:24:12'),
(29, 12, 11, 'BRIGANTIA Talla M', 'DE110T-M', '79.00', 0, '2018-12-07 06:24:12'),
(30, 12, 8, 'BRIGANTIA Talla L', 'DE110T-L', '79.00', 0, '2018-12-07 06:32:08'),
(31, 13, 10, 'SIERRA Talla M', 'DE111T-M', '79.00', 0, '2018-12-07 06:32:08'),
(32, 13, 8, 'SIERRA Talla L', 'DE111T-L', '79.00', 0, '2018-12-07 06:34:36'),
(33, 2, 0, 'ISA Talla M', 'DE101TP-M', '74.00', 0, '2018-12-07 06:38:45'),
(34, 2, 0, 'ISA Talla L', 'DE101TP-L', '74.00', 0, '2018-12-07 06:38:45'),
(35, 6, 0, 'GESA Talla L', 'DE105T-L', '74.00', 0, '2018-12-07 06:40:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_variants_values`
--

CREATE TABLE `products_variants_values` (
  `id_products_variants_values` int(15) NOT NULL,
  `id_products_variants_products_variants_values` int(15) NOT NULL,
  `id_products_attr_values_products_variants_values` int(15) NOT NULL,
  `created_products_variants_values` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `products_variants_values`
--

INSERT INTO `products_variants_values` (`id_products_variants_values`, `id_products_variants_products_variants_values`, `id_products_attr_values_products_variants_values`, `created_products_variants_values`) VALUES
(1, 1, 1, '2018-11-06 15:56:24'),
(2, 2, 2, '2018-11-06 15:56:24'),
(4, 3, 1, '2018-11-06 17:16:07'),
(5, 5, 1, '2018-11-24 15:12:09'),
(6, 6, 1, '2018-11-27 01:44:25'),
(7, 7, 1, '2018-11-27 01:44:25'),
(8, 8, 1, '2018-11-27 01:46:01'),
(9, 9, 1, '2018-11-27 01:46:01'),
(10, 10, 1, '2018-11-28 01:28:05'),
(11, 11, 1, '2018-11-28 01:28:05'),
(12, 28, 2, '2018-11-28 01:28:18'),
(13, 12, 1, '2018-11-28 01:28:18'),
(14, 13, 1, '2018-11-28 01:28:35'),
(15, 14, 1, '2018-11-28 01:28:35'),
(16, 18, 2, '2018-12-05 23:23:12'),
(17, 19, 3, '2018-12-05 23:23:12'),
(18, 15, 2, '2018-12-05 23:24:50'),
(19, 26, 2, '2018-12-05 23:25:42'),
(20, 27, 3, '2018-12-05 23:25:42'),
(21, 20, 2, '2018-12-05 23:29:14'),
(22, 21, 3, '2018-12-05 23:29:14'),
(23, 24, 2, '2018-12-05 23:29:14'),
(24, 25, 3, '2018-12-05 23:29:14'),
(25, 29, 2, '2018-12-05 23:29:14'),
(26, 29, 2, '2018-12-05 23:29:14'),
(27, 30, 3, '2018-12-05 23:29:14'),
(28, 33, 2, '2018-12-05 23:29:14'),
(29, 31, 3, '2018-12-05 23:29:14'),
(30, 35, 3, '2018-12-05 23:29:14'),
(31, 34, 3, '2018-12-05 23:29:25'),
(32, 22, 2, '2018-12-11 04:42:22'),
(33, 23, 3, '2018-12-11 04:42:22'),
(34, 32, 3, '2018-12-11 04:43:35');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_orders`),
  ADD KEY `id_persons_orders` (`id_persons_orders`);

--
-- Indices de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`id_orders_products`),
  ADD KEY `id_orders_orders_products` (`id_orders_orders_products`),
  ADD KEY `id_products_variants_orders_products` (`id_products_variants_orders_products`);

--
-- Indices de la tabla `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id_persons`),
  ADD UNIQUE KEY `email_persons` (`email_persons`);

--
-- Indices de la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  ADD PRIMARY KEY (`id_persons_admin`),
  ADD KEY `id_persons_persons_admin` (`id_persons_persons_admin`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_products`),
  ADD UNIQUE KEY `code_products` (`code_products`);

--
-- Indices de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id_products_attributes`);

--
-- Indices de la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  ADD PRIMARY KEY (`id_products_attr_values`),
  ADD KEY `id_products_attributes_products_attr_values` (`id_products_attributes_products_attr_values`);

--
-- Indices de la tabla `products_categories`
--
ALTER TABLE `products_categories`
  ADD PRIMARY KEY (`id_products_categories`);

--
-- Indices de la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  ADD PRIMARY KEY (`id_products_gallery`),
  ADD KEY `id_products_products_galellery` (`id_products_products_gallery`);

--
-- Indices de la tabla `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id_products_images`),
  ADD KEY `id_products_variants_products_images` (`id_products_variants_products_images`);

--
-- Indices de la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  ADD PRIMARY KEY (`id_products_in_categories`),
  ADD KEY `id_products_categories_products_in_categories` (`id_products_categories_products_in_categories`);

--
-- Indices de la tabla `products_variants`
--
ALTER TABLE `products_variants`
  ADD PRIMARY KEY (`id_products_variants`),
  ADD UNIQUE KEY `code_products_variants` (`code_products_variants`),
  ADD KEY `id_products_products_variants` (`id_products_products_variants`);

--
-- Indices de la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  ADD PRIMARY KEY (`id_products_variants_values`),
  ADD KEY `id_products_variants_products_variants_values` (`id_products_variants_products_variants_values`),
  ADD KEY `id_products_attr_values_products_variants_values` (`id_products_attr_values_products_variants_values`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id_orders` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `id_orders_products` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `persons`
--
ALTER TABLE `persons`
  MODIFY `id_persons` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  MODIFY `id_persons_admin` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id_products` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id_products_attributes` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  MODIFY `id_products_attr_values` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `products_categories`
--
ALTER TABLE `products_categories`
  MODIFY `id_products_categories` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  MODIFY `id_products_gallery` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id_products_images` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  MODIFY `id_products_in_categories` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `products_variants`
--
ALTER TABLE `products_variants`
  MODIFY `id_products_variants` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  MODIFY `id_products_variants_values` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_persons_orders`) REFERENCES `persons` (`id_persons`);

--
-- Filtros para la tabla `orders_products`
--
ALTER TABLE `orders_products`
  ADD CONSTRAINT `orders_products_ibfk_1` FOREIGN KEY (`id_orders_orders_products`) REFERENCES `orders` (`id_orders`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_products_ibfk_2` FOREIGN KEY (`id_products_variants_orders_products`) REFERENCES `products_variants` (`id_products_variants`);

--
-- Filtros para la tabla `persons_admin`
--
ALTER TABLE `persons_admin`
  ADD CONSTRAINT `persons_admin_ibfk_1` FOREIGN KEY (`id_persons_persons_admin`) REFERENCES `persons` (`id_persons`);

--
-- Filtros para la tabla `products_attr_values`
--
ALTER TABLE `products_attr_values`
  ADD CONSTRAINT `products_attr_values_ibfk_1` FOREIGN KEY (`id_products_attributes_products_attr_values`) REFERENCES `products_attributes` (`id_products_attributes`);

--
-- Filtros para la tabla `products_gallery`
--
ALTER TABLE `products_gallery`
  ADD CONSTRAINT `products_gallery_ibfk_1` FOREIGN KEY (`id_products_products_gallery`) REFERENCES `products` (`id_products`);

--
-- Filtros para la tabla `products_images`
--
ALTER TABLE `products_images`
  ADD CONSTRAINT `products_images_ibfk_1` FOREIGN KEY (`id_products_variants_products_images`) REFERENCES `products_variants` (`id_products_variants`);

--
-- Filtros para la tabla `products_in_categories`
--
ALTER TABLE `products_in_categories`
  ADD CONSTRAINT `products_in_categories_ibfk_1` FOREIGN KEY (`id_products_categories_products_in_categories`) REFERENCES `products` (`id_products`),
  ADD CONSTRAINT `products_in_categories_ibfk_2` FOREIGN KEY (`id_products_categories_products_in_categories`) REFERENCES `products_categories` (`id_products_categories`);

--
-- Filtros para la tabla `products_variants`
--
ALTER TABLE `products_variants`
  ADD CONSTRAINT `products_variants_ibfk_1` FOREIGN KEY (`id_products_products_variants`) REFERENCES `products` (`id_products`);

--
-- Filtros para la tabla `products_variants_values`
--
ALTER TABLE `products_variants_values`
  ADD CONSTRAINT `products_variants_values_ibfk_1` FOREIGN KEY (`id_products_variants_products_variants_values`) REFERENCES `products_variants` (`id_products_variants`),
  ADD CONSTRAINT `products_variants_values_ibfk_2` FOREIGN KEY (`id_products_attr_values_products_variants_values`) REFERENCES `products_attr_values` (`id_products_attr_values`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
