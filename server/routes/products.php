<?php
/**
 * @function router
 * @description Rutas de la entidad producto
 * @param {string} $route Ruta solicitada
 * @param {string} $controllersDir ubicacion de la carpeta de controladores
 * @param {string} $modelsDir Ubicacion de la carpeta de modelos
 */
function router ($route, $controllersDir, $modelsDir) {
    require_once($controllersDir.'products'.'.php');
    include_once('../server/routes/errors.php');
    $method = $_SERVER['REQUEST_METHOD'];
    $products = new ProductsController();

    switch ($route) {
        case '/':
            switch ($method) {
                case 'GET':
                    $all = null;
                    return $products->readProducts($all);
                    break;
                // case 'POST':
                //     // Ruta protegida por login
                //     require_once('../server/utils/seguridad.php');
                //     check_usr_ok();
                //     return $products->createProducts();
                //     break;
                default:
                    /**
                     * @todo Crear manejador de Not Found 404
                     */
                    return $res['error'] = "Método inválido. rp:".$method;
                    break;
            }
            break;
        case '/all':
            switch ($method) {
                case 'GET':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    $all = "true";
                    return $products->readProducts($all);
                    break;
                default:
                    /**
                     * @todo Crear manejador de Not Found 404
                     */
                    return $res['error'] = "Método inválido. rp:".$method;
                    break;
            }
            break;
        case ':id':
            $id = $_GET['id'];
            switch ($method) {
                case 'GET':
                    return $products->readProductsImages($id);
                    # code...
                    break;
                case 'PUT':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    parse_str(file_get_contents("php://input"),$post_vars);
                    $disabled = $post_vars['disabled'];
                    return $products->updateProductsDisabled($id,$disabled);
                    # code...
                    break;
                default:
                    # code...
                    $res['error'] = "Método inválido. rp2: ".$method;
                    return $res;
                    break;
            }
            break;
        case ':id/all':
            $id = $_GET['id'];
            switch ($method) {
                case 'GET':
                    return $products->readProductAll($id);
                    # code...
                    break;
                default:
                    # code...
                    $res['error'] = "Método inválido. rp3: ".$method;
                    return $res;
                    break;
            }
            break;
        default:
            # code...
            $res['error'] = "Ruta inválida. rp: ".$route;
            return $res;
            break;
    }
}
?>