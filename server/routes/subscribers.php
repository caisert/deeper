<?php
/**
 * @function router
 * @description Rutas de la entidad suscriptor
 * @param {string} $route Ruta solicitada
 * @param {string} $controllersDir ubicacion de la carpeta de controladores
 * @param {string} $modelsDir Ubicacion de la carpeta de modelos
 */
function router ($route, $controllersDir, $modelsDir) {
    require_once($controllersDir.'subscribers'.'.php');
    include_once('../server/routes/errors.php');
    $method = $_SERVER['REQUEST_METHOD'];
    $subscribers = new SubscribersController();

    switch ($route) {
        case '/':
            switch ($method) {
                case 'GET':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    return $subscribers->readSubscribers();
                    break;
                case 'POST':
                    $subscriber = $_POST['subscriber'];
                    return $subscribers->createSubscriber($subscriber);
                    break;
                default:
                    /**
                     * @todo Crear manejador de Not Found 404
                     */
                    return $res['error'] = "Método inválido. rp:".$method;
                    break;
            }
            break;
        case ':id':
            $id = $_GET['id'];
            switch ($method) {
                case 'GET':
                    return $subscribers->readSubscribersImages($id);
                    # code...
                    break;
                case 'PUT':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    return $subscribers->updateSubscribersDisabled($id);
                    # code...
                    break;
                default:
                    # code...
                    $res['error'] = "Método inválido. rp2: ".$method;
                    return $res;
                    break;
            }
            break;
            case '/contactus':
                switch ($method) {
                    case 'POST':
                        return $subscribers->sendContactForm($_POST['contact']);
                        # code...
                        break;
                    default:
                        # code...
                        $res['error'] = "Método inválido. rp3: ".$method;
                        return $res;
                        break;
                }
                break;
            
                case '/changePassword':
                switch ($method) {
                    case 'POST':
                        // Ruta protegida por login
                        require_once('../server/utils/seguridad.php');
                        check_usr_ok();
                        return $subscribers->changePassword($_POST['password']);
                        # code...
                        break;
                    default:
                        # code...
                        $res['error'] = "Método inválido. rp3: ".$method;
                        return $res;
                        break;
                }
                break;
        default:
            # code...
            $res['error'] = "Ruta inválida. rp: ".$route;
            return $res;
            break;
    }
}
?>