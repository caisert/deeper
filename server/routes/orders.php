<?php
/**
 * @function router
 * @description Rutas de la entidad orders
 * @param {string} $route Ruta solicitada
 * @param {string} $controllersDir ubicacion de la carpeta de controladores
 * @param {string} $modelsDir Ubicacion de la carpeta de modelos
 */
function router ($route, $controllersDir, $modelsDir) {
    require_once($controllersDir.'orders'.'.php');
    include_once('../server/routes/errors.php');
    $method = $_SERVER['REQUEST_METHOD'];
    $orders = new OrdersController();

    switch ($route) {
        case '/':
            switch ($method) {
                case 'GET':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    $all = null;
                    return $orders->readOrders($all);
                    break;
                case 'POST':
                    return $orders->createOrders($_POST['order']);
                    break;
                case 'PUT':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    parse_str(file_get_contents("php://input"),$post_vars);
                    $order = $post_vars['order'];
                    return $orders->updateOrders($order);
                    # code...
                    break;
                case 'DELETE':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    return $orders->deleteOrders($_GET['id']);
                    # code...
                    break;
                default:
                    /**
                     * @todo Crear manejador de Not Found 404
                     */
                    return $res['error'] = "Método inválido. rp:".$method;
                    break;
            }
            break;
        case ':id':
            $id = $_GET['id'];
            switch ($method) {
                case 'GET':
                    // return $orders->readOrder($id);
                    # code...
                    break;
                case 'PUT':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    return $orders->updateOrder($_POST['order']);
                    # code...
                    break;
                case 'DELETE':
                    // Ruta protegida por login
                    require_once('../server/utils/seguridad.php');
                    check_usr_ok();
                    return $orders->deleteOrder($_GET['id']);
                    # code...
                    break;
                default:
                    # code...
                    $res['error'] = "Método inválido. rp2: ".$method;
                    return $res;
                    break;
            }
            break;
        default:
            # code...
            $res['error'] = "Ruta inválida. ro: ".$route;
            return $res;
            break;
    }
}
?>