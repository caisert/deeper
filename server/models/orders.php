<?php
/**
 * @class OrdersModel
 * @constructor PDO
 * @description Modelo de la entidad Orders
 */
class OrdersModel
{
    // Declaración de una propiedad
    private $db;

    // Constructor
    public function __construct() {
        include 'database.php';
        $this->db = dbInit();
    }
    // Declaración de un método
    public function mostrarVar() {
        echo $this->var;
    }
    // $this->db->errorCode() 
    /**
     * @function readOrders
     * @description Leer la lista de ordenes disponibles.
     */
    public function readOrders($all = false) {
        // $disponibles = (!$all) ? ' WHERE quantity_products_variants > 0 ' : '' ;
        $sql = 'SELECT orders.*, persons.*, id_orders_products, price_orders_products, quantity_orders_products, gift_orders_products, name_products_variants, code_products_variants FROM orders JOIN persons ON id_persons_orders = id_persons JOIN orders_products ON id_orders = id_orders_orders_products JOIN products_variants ON id_products_variants_orders_products = id_products_variants ORDER BY date_orders DESC';
        $statement = $this->db->prepare($sql);
        $statement->execute();
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $orders = [];
            $lastOrderId = NULL;
            while ($fila = $statement->fetchObject()) {
                if ( $lastOrderId != $fila->id_orders ) {
                    if ( $lastOrderId !== NULL ) {
                        // add this obj to the array
                        $orders[] = $tOrder;
                    }
                    // start a new temp object
                    $tOrder = new stdClass();
                    $tOrder->id_orders = $fila->id_orders;
                    $tOrder->address_orders = $fila->address_orders;
                    $tOrder->city_orders = $fila->city_orders;
                    $tOrder->country_orders = $fila->country_orders;
                    $tOrder->date_orders = $fila->date_orders;
                    $tOrder->email_persons = $fila->email_persons;
                    $tOrder->last_persons = $fila->last_persons;
                    $tOrder->method_orders = $fila->method_orders;
                    $tOrder->name_persons = $fila->name_persons;
                    $tOrder->paypal_orders = $fila->paypal_orders;
                    $tOrder->phone_orders = $fila->phone_orders;
                    $tOrder->phone_persons = $fila->phone_persons;
                    $tOrder->status_orders = $fila->status_orders;
                    $tOrder->total_amount_orders = $fila->total_amount_orders;
                    $tOrder->tracking_orders = $fila->tracking_orders;
                    $tOrder->zip_orders = $fila->zip_orders;
                    // // $tOrder->carrier_orders = $fila->carrier_orders;
                    $tOrder->products = [];
                }

                // Add product to current temp obj
                $tOrder->products[] = [
                    'id'=>$fila->id_orders_products, 
                    'name'=>$fila->name_products_variants, 
                    'code' => $fila->code_products_variants, 
                    'quantity'=>$fila->quantity_orders_products,
                    'gift'=>$fila->gift_orders_products,
                    'price'=>$fila->price_orders_products
                ];

                $lastOrderId = $fila->id_orders;
            }
            // Añadir el último elemento
            $orders[] = $tOrder;
            $res['load'] = $orders;
        }
        return $res;
    }

    public function createOrders($order){
        // print_r($order);
        if (!array_key_exists('items', $order) || count($order['items']) == 0){
            $res['error'] = "Select at least one product.";
        // } elseif () {

        } else {
            $statement = $this->db->prepare('INSERT INTO persons (`name_persons`,`last_persons`,`email_persons`,`phone_persons`) VALUES (:name_persons, :last_persons, :email_persons, :phone_persons)');
            $statement->execute([
                'name_persons' => $order['customer']['name'],
                'last_persons' => $order['customer']['surname'],
                'email_persons' => $order['customer']['email'],
                'phone_persons' => $order['customer']['phone'],
            ]);
            // print_r ($statement->errorInfo());
            $id_persons = $this->db->lastInsertId();
            if ($id_persons == 0) {
                $statement = $this->db->prepare('SELECT id_persons FROM persons WHERE email_persons = ?');
                $statement->execute( [$order['customer']['email'] ]);
                $id_persons = $statement->fetchObject()->id_persons;
            }
            // echo " id_persons: ".$id_persons;
            $statement = $this->db->prepare('INSERT INTO `orders`(`id_persons_orders`, `address_orders`, `city_orders`, `zip_orders`, `country_orders`, `phone_orders`, `status_orders`, `total_amount_orders`, `paypal_orders`, `method_orders`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
            $status = 'Not yet shipped';// En caso de modificar, hacerlo tambien en orders.vue
            $statement->execute([
                /* ':id_persons_orders' => */ $id_persons,
                /* ':address_orders' => */ $order['shipping']['address'],
                /* ':city_orders' => */ $order['shipping']['city'],
                /* ':zip_orders' => */ $order['shipping']['zip'],
                /* ':country_orders' => */ $order['shipping']['country'],
                /* ':phone_orders' => */ $order['shipping']['phone'],
                /* ':status_orders' => $order['shipping'][''], */ $status,
                /* ':total_amount_orders' => */ $order['bill']['amount'],
                /* ':paypal_orders' => */ $order['bill']['paymentID'],
                /* ':method_orders' => */ $order['bill']['method']
            ]);
            // print_r ($statement->errorInfo());
            $id_order = $this->db->lastInsertId();
            $sql = 'INSERT INTO `orders_products`(`id_orders_orders_products`, `id_products_variants_orders_products`,`price_orders_products`, `quantity_orders_products`, `gift_orders_products`) VALUES ';
            $items = [];
            $coma = "";
            foreach ($order['items'] as $item) {
                $gift = $item['wantGift'] == "true" ? $item['gift'] : '0';
                // echo " #variante>".$item['name_products_variants'];
                $sql .= $coma."(?, ?, ?, ?, ?)";
                array_push($items, $id_order, $item['id_products_variants'], $item['price_products_variants'], $item['quantity'], $gift);
                $coma = ",";
            }
            // echo $sql;
            // print_r($items);
            $statement = $this->db->prepare($sql);
            $statement->execute($items);
            // print_r ($statement->errorInfo());
            /**
             * @todo modulizar el siguiente if else
             */
            if ($statement->errorInfo()[0] != 0) {
                $res['error'] = $statement->errorInfo();
            } else {
                $load = ["order" => "$id_order"];
                /* while ($fila = $statement->fetchObject()) {
                $load[] = $fila;
                } */
                $res['load'] = $load;
            }
        }
        return $res;
    }
    
    public function updateOrders($order){
        $statement = $this->db->prepare('UPDATE orders SET `tracking_orders`= :tracking, `status_orders`= :status WHERE id_orders = :id');
        $statement->execute([
            ':id' => $order['id_orders'],
            ':tracking' => $order['tracking_orders'],
            ':status' => $order['status_orders']
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $res['load'] = "OK";
        }
        return $res;
    }
    
    public function deleteOrders($order){
        $statement = $this->db->prepare('DELETE FROM orders  WHERE id_orders = :id');
        $statement->execute([
            ':id' => $order,
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $res['load'] = "OK";
        }
        return $res;
    }
}
?>