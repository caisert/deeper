<?php
function dbInit() {
    $db_conf = json_decode(file_get_contents('../localconf/database.json'), true);
    
    $dbhost = $db_conf['host'];
    $dbuser = $db_conf['user'];
    $dbpass = $db_conf['password'];
    $dbname = $db_conf['database'];
    $dbcharset = $db_conf['charset'];
    
    try {
        $db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname.';charset='.$dbcharset, $dbuser, $dbpass);
    }
    catch (PDOException $e){
        die('No se pudo conectar a la BD.');
        // die('No se pudo conectar a la BD: '.$e->getMessage());
    }
    return $db;
}

?>
