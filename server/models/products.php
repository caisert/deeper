<?php
/**
 * @class ProductsModel
 * @constructor PDO
 * @description Modelo de la entidad Productos
 */
class ProductsModel
{
    // Declaración de una propiedad
    private $db;

    // Constructor
    public function __construct() {
        include 'database.php';
        $this->db = dbInit();
    }
    // Declaración de un método
    public function mostrarVar() {
        echo $this->var;
    }
    // $this->db->errorCode() 
    /**
     * @function readProducts
     * @description Leer la lista de productos disponibles.
     */
    public function readProducts($all = false) {
        $disponibles = (!$all) ? ' WHERE quantity_products_variants > 0 AND disabled_products = 0' : '' ;
        
        $sql = 'SELECT id_products, name_products, price_products, image_products, disabled_products, SUM(quantity_products_variants) AS quantity FROM products JOIN products_variants ON id_products=id_products_products_variants JOIN products_in_categories on id_products = id_products_products_in_categories JOIN products_categories ON id_products_categories_products_in_categories = id_products_categories '.$disponibles.' GROUP BY id_products_products_variants ORDER BY order_products_categories,code_products';
        
        $statement = $this->db->prepare($sql);
        $statement->execute();
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }

    public function readProductDetails($id){
        $statement = $this->db->prepare('SELECT * FROM products WHERE id_products = :id');
        $statement->execute([ ':id' => $id]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $load = [];
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }

    public function readProductVariants($id) {
        $statement = $this->db->prepare('SELECT * FROM products_variants LEFT JOIN products_variants_values ON id_products_variants = id_products_variants_products_variants_values LEFT JOIN products_attr_values ON id_products_attr_values_products_variants_values = id_products_attr_values LEFT JOIN products_attributes ON id_products_attributes_products_attr_values = id_products_attributes WHERE id_products_products_variants = :id AND disabled_products_variants= :disabled ORDER BY id_products_variants');
        $statement->execute([
            ':id' => $id,
            ':disabled' => 0
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $load = [];
            $variants = [];
            $attributes = [];
            $lastVariantId = NULL;
            while ($fila = $statement->fetchObject()) {
                if ( $lastVariantId != $fila->id_products_variants ) {
                    if ( $lastVariantId !== NULL ) {
                        // add this obj to the post array
                        $variants[] = $tVariant;
                    }
                    // start a new temp object
                    $tVariant          = new stdClass();
                    $tVariant->id_products_variants = $fila->id_products_variants;
                    $tVariant->name_products_variants = $fila->name_products_variants;
                    $tVariant->price_products_variants = $fila->price_products_variants;
                    $tVariant->quantity_products_variants = $fila->quantity_products_variants;
                    $tVariant->attrs  = [];
                }

                // Add photo_id to current tVariant obj
                $tVariant->attrs[] = [
                    'id'=>$fila->id_products_attributes, 
                    'id_av'=>$fila->id_products_attr_values_products_variants_values, 
                    'name' => $fila->name_products_attributes, 
                    'value'=>$fila->name_products_attr_values
                ];

                $lastVariantId = $fila->id_products_variants;
                $attributes[] = ['id'=>$fila->id_products_attributes, 'name'=>$fila->name_products_attributes];
            }
            // Añadir el último elemento
            $variants[] = $tVariant;
            // Filtrar atributos para que no se repitan
            function unique_multidim_array($array, $key) { 
                $temp_array = array(); 
                $i = 0; 
                $key_array = array(); 
                
                foreach($array as $val) { 
                    if (!in_array($val[$key], $key_array)) { 
                        $key_array[$i] = $val[$key]; 
                        $temp_array[$i] = $val; 
                    } 
                    $i++; 
                } 
                return $temp_array; 
            } 
            $load['variants'] = $variants;
            $load['attributes'] =unique_multidim_array($attributes,'id'); 
            // $load['attributes'] = $attributes;
            $res['load'] = $load;
        }
        return $res;
    }

    public function readProductImages($id) {
        $statement = $this->db->prepare('SELECT products_images.* FROM products_variants LEFT JOIN products_images ON id_products_variants = id_products_variants_products_images WHERE id_products_products_variants = :id AND disabled_products_images= :disabled');
        $statement->execute([
            ':id' => $id,
            ':disabled' => 0
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $load = [];
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }
    public function readProductsSlides($id) {
        $statement = $this->db->prepare('SELECT * FROM products_gallery WHERE id_slider_products_gallery = :id AND disabled_products_gallery= :disabled ORDER BY order_products_gallery ASC');
        $statement->execute([
            ':id' => $id,
            ':disabled' => 0
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $load = [];
            while ($fila = $statement->fetchObject()) {
              $load[] = $fila;
            }
            $res['load'] = $load;
        }
        return $res;
    }
    public function updateProductsDisabled($id,$disabled) {
        $statement = $this->db->prepare('UPDATE products SET `disabled_products`= :disabled WHERE id_products = :id');
        $statement->execute([
            ':id' => $id,
            ':disabled' => $disabled
        ]);
        /**
         * @todo modulizar el siguiente if else
         */
        if ($statement->errorInfo()[0] != 0) {
            $res['error'] = $statement->errorInfo();
        } else {
            $res['load'] = "OK";
        }
        return $res;
    }
}
?>