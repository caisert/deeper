<?php
require($modelsDir.'products'.'.php');

/**
 * @class ProductsController
 * @description Controlador de la entidad productos
 */
class ProductsController
{
    // Declaración de una propiedad. Ejemplo
    // public $var = 'un valor predeterminado c';

    // Declaración de un método
    public function readProducts($all) {
        $m = new ProductsModel();
        return $m->readProducts($all);
    }
    // Declaración de un método
    public function readProductsImages($id) {
        $m = new ProductsModel();
        return $m->readProductImages($id);
    }
    public function readProductsSlides($id) {
        $m = new ProductsModel();
        return $m->readProductsSlides($id);
    }
    public function readProductAll($id) {
        $m = new ProductsModel();
        $res = [];
        $res['details'] = $m->readProductDetails($id);
        $res['variants'] = $m->readProductVariants($id);
        $res['images'] = $m->readProductImages($id);

        return $res;
    }
    public function updateProductsDisabled($id,$disabled) {
        $m = new ProductsModel();
        return $m->updateProductsDisabled($id,$disabled);
    }
}
?>