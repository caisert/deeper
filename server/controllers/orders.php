<?php
require($modelsDir.'orders'.'.php');

/**
 * @class OrdersController
 * @description Controlador de la entidad orders
 */
class OrdersController
{
    // Declaración de una propiedad. Ejemplo
    // public $var = 'un valor predeterminado c';

    // Declaración de un método
    public function createOrders($order) {
        $m = new OrdersModel();
        // return $m->createOrders($order);
        $res = $m->createOrders($order);
        if (!isset($res['error'])) {
            $this->sendOrdersEmails($order, $res['load']['order']);
        }
        return $res;
    }
    public function readOrders($order) {
        $m = new OrdersModel();
        return $m->readOrders($order);
    }
    public function updateOrders($order) {
        $m = new OrdersModel();
        return $m->updateOrders($order);
    }
    public function deleteOrders($order) {
        $m = new OrdersModel();
        return $m->deleteOrders($order);
    }

    private function sendOrdersEmails($order, $id_order) {
        $email_owner = "angelo@deeperclothing.com";// "rdimicheleb@hotmail.com";
        $email_from = "info@deeperclothing.com";

        $items = "<ul>";
        foreach ($order['items'] as $item) {
            $items .= "<li>";
            $items .= $item['quantity_products_variants'];
            $items .= " * ";
            $items .= $item['name_products_variants'];
            $items .= ", price: $";
            $items .= $item['price_products_variants'];
            $items .= ", gift wrap: ";
            $items .= $item['wantGift'] == "true" ? $item['gift'] : '0';
            $items .= "</li>";
        }
        $items .= "</ul>";

        $datos_pedido = " <b>Order ID:</b> ".$id_order."\r\n ".
        " <br><b>Customer:</b> ".$order['customer']['name']." ".$order['customer']['surname'].
        "\r\n <br><b>Tel:</b> ".$order['customer']['phone']."\r\n <br><b>E-mail: </b>".$order['customer']['email'].
        "\r\n <br><b>Address:</b> ".$order['shipping']['address'].", city: ".$order['shipping']['city'].
        ", ZIP: ".$order['shipping']['zip'].", phone:".$order['shipping']['phone']."\r\n ".
        "\r\n <br> <b>Products:</b> <br>".$items.
        "\r\n Total:<b> $".$order['bill']['amount']."</b>";

        // Email para el representante de la tienda
        $para = $email_owner;
        $asunto = "Tu pedido en DEEPER Clothing. ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: DEEPER Clothing <'.$email_from.'>' . "\r\n";
        $headers .= 'Reply-To: '.$email_from.' \r\n';

        $msj = "New order received: <br><br>\r\n". $datos_pedido;
        $msj .= " <br><br> Remember to send the carrier's name and tracking number to the client.";
        // $msj .= " <br><img src='localhost/mercaito/CSS/logo1.png' width='100' height='100'> ";

        if(!mail($para, $asunto, $msj, $headers)){}
        // echo $msj;

        // Email para el comprador
        $para = $order['customer']['email'];
        $asunto = "Tu pedido en DEEPER Clothing. ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: DEEPER Clothing <'.$email_from.'>' . "\r\n";
        $headers .= 'Reply-To: '.$email_from.' \r\n';
        $msj = "We received your order: <br><br>\r\n". $datos_pedido;
        $msj .= " <br><br> We will send you soon the carrier's name and tracking number.";
        if(!mail($para, $asunto, $msj, $headers)){}
        // echo $msj;
    }
}
?>