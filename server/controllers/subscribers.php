<?php
require($modelsDir.'subscribers'.'.php');

/**
 * @class SubscribersController
 * @description Controlador de la entidad suscriptores
 */
class SubscribersController
{
    // Declaración de una propiedad. Ejemplo
    // public $var = 'un valor predeterminado c';

    // Declaración de un método
    public function readSubscribers() {
        $m = new SubscribersModel();
        return $m->readSubscribers();
    }
    // Declaración de un método
    public function createSubscriber($subscriber) {
        $m = new SubscribersModel();
        return $m->createSubscriber($subscriber);
    }
    // Declaración de un método
    public function changePassword($password) {
        $m = new SubscribersModel();
        return $m->changePassword($password);
    }
    // Declaración de un método
    public function sendContactForm($message) {
        $email_owner = "rdimicheleb@hotmail.com";
        $email_from = $message['email'];
        // Email para el representante de la tienda
        $para = $email_owner;
        $asunto = "Mensaje desde web DEEPER Clothing. ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: DEEPER Clothing <'.$email_from.'>' . "\r\n";
        $headers .= 'Reply-To: '.$email_from.' \r\n';

        $msj = "New message received: <br><br>\r\n";
        $msj .= " <br><br> Message from: ".$message['name']." ".$message['email'];
        $msj .= " <br><br> Country: ".$message['country'];
        $msj .= " <br><br> Message: ".$message['message'];

        if(!mail($para, $asunto, $msj, $headers)){
            $res['error'] = "Message not send.";
        }
        $res['load'] = "ok";
        // echo $msj;
        
        return $res;
    }
}
?>